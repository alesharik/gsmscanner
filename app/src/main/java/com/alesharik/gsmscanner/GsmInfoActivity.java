package com.alesharik.gsmscanner;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.CellInfo;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.Timer;
import java.util.TimerTask;

public class GsmInfoActivity extends AppCompatActivity implements Runnable {
    private CellInfo info;
    private Timer timer;
    private LineGraphSeries<DataPoint> series;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gsm_info);

        info = getIntent().getParcelableExtra("info");

        series = new LineGraphSeries<>();
        series.setTitle("Signal (dbm)");
        GraphView v = findViewById(R.id.graph);
        v.addSeries(series);
        v.getViewport().setMinX(0);
        v.getViewport().setMaxX(30);
        v.getLegendRenderer().setVisible(true);
        v.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);

        timer = new Timer(true);
        timer.scheduleAtFixedRate(new UpdateTask(this), 0, 1000);

        TextView textView = findViewById(R.id.gsm_name);
        textView.setText(info.toString());

        Button button = findViewById(R.id.btn_back);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void run() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 10);
            return;
        }
        if (telephonyManager != null) {
            for (CellInfo i : telephonyManager.getAllCellInfo()) {
                if (i instanceof CellInfoGsm && info instanceof CellInfoGsm) {
                    if (((CellInfoGsm) i).getCellIdentity().equals(((CellInfoGsm) info).getCellIdentity())) {
                        series.appendData(new DataPoint(0, ((CellInfoGsm) i).getCellSignalStrength().getDbm()), true, 30);
                        break;
                    }
                } else if (i instanceof CellInfoLte && info instanceof CellInfoLte) {
                    if (((CellInfoLte) i).getCellIdentity().equals(((CellInfoLte) info).getCellIdentity())) {
                        series.appendData(new DataPoint(0, ((CellInfoLte) i).getCellSignalStrength().getDbm()), true, 30);
                        break;
                    }
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.purge();
    }

    private static final class UpdateTask extends TimerTask {
        private final Runnable runnable;

        private UpdateTask(Runnable runnable) {
            this.runnable = runnable;
        }

        @Override
        public void run() {
            runnable.run();
        }
    }
}
